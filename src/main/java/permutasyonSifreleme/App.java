package permutasyonSifreleme;


import java.util.*;

public class App {
    public static void main(String [] args){

        System.out.println("Kelime gir : ");
        Scanner scanner = new Scanner(System.in);
        String kelime = scanner.nextLine();
        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Anahtar gir : ");
        int anahtar = scanner2.nextInt();
        List<Character> characterList = getCharacterList(kelime,anahtar);
        List<Integer> integerList  = getIntegerList(anahtar);

        int sutun = anahtar;
        int satir  = characterList.size() / sutun;
        int sayac = 0;
        char  matris [][] =new char[satir][sutun];
        for (int i = 0 ; i < satir ; i ++ ){
            for (int j=0; j< sutun ; j++ ){
                matris[i][j]  = characterList.get(sayac);
                sayac++ ;
                System.out.print(matris[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        char  sifrelenmisMatris [][] =new char[satir][sutun];
        for (int i = 0 ; i < satir ; i ++ ){
            for (int j=0; j< sutun ; j++ ){
                sifrelenmisMatris [i][j]  = matris[i][integerList.get(j)];
                System.out.print(sifrelenmisMatris[i][j]);
            }
        }


    }

    private static List<Character> getCharacterList(String kelime ,int anahtar) {
        List<Character> characterList = new ArrayList<Character>();

        for (int i = 0 ; i < kelime.length() ; i++ ){
            characterList.add(kelime.charAt(i));
        }

        while (characterList.size() % anahtar !=0){
            characterList.add('-');
        }
    return characterList;

    }


    public static List<Integer> getIntegerList (int anahtar){

        List<Integer> integerList = new ArrayList<Integer>();
        int sayac = 0 ;
        do {
            Random random = new Random();
            int randomSayi = random.nextInt(anahtar);
            if (!integerList.contains(randomSayi)){
                integerList.add(randomSayi);
                sayac++;
            }
        }while (sayac != anahtar);
        return integerList;
    }

}
