package yerineKoyma;

import java.util.*;

public class App {


    private static final String ALFABE = "abcçdefgğhıijklmnoöprsştuüvyzwqx";
    public static void main(String[] args){
        List<Integer> integerList = getRandomIntegerList();
        List<String> stringList = getStringList(integerList);
        HashMap<String,String> stringHashpMap = getStringHashpMap(stringList);

        System.out.print("Kelime gir : ");
        Scanner scanner = new Scanner(System.in);
        String kelime = scanner.nextLine();
        List<Character> characterList = getCharacterList(kelime);
        for (Character character : characterList){
            System.out.print(stringHashpMap.get(String.valueOf(character)));
        }
        System.out.println();
        System.out.println("Çözmek için metin gir : ");
        String şifreliMetin = scanner.nextLine().toLowerCase();
        HashMap<String,String> stringHashpMapSolve = getStringHashpMapSolve(stringList);

         List<Character> characters = getCharacterList(şifreliMetin);
         for (Character character : characters){
            System.out.print(stringHashpMapSolve.get(String.valueOf(character)));
         }
    }
    public static List<Character> getCharacterList(String kelime){
        List<Character> characters = new ArrayList<Character>();
        for (int i = 0;i<kelime.length() ; i++){
            characters.add(kelime.charAt(i));
        }
        return characters;
    }
    public static List<Integer> getRandomIntegerList() {
        List<Integer> integerList = new ArrayList<Integer>();
        int sayac = 0;
        do {
            Random rnd = new Random();
            int randomRakam = rnd.nextInt(32);
            randomRakam = (int) Math.ceil(randomRakam);
            if (!integerList.contains(randomRakam) && randomRakam >= 0) {
                integerList.add(randomRakam);
                sayac++;
            }
        } while (sayac != 32);
        return integerList;
    }
    public static List<String> getStringList(List<Integer> integerList){
        HashMap<String,Integer> stringIntegerHashMap = new HashMap<String, Integer>();
        List<String> stringList = new ArrayList<String>();
        for (int i =0 ;i<32 ; i++){
        stringList.add(String.valueOf(ALFABE.charAt(integerList.get(i))));
        }
        return stringList;
    }
    public static HashMap<String,String> getStringHashpMap(List<String> stringList){
        HashMap<String,String> hashMapList = new HashMap<String, String>();

        for (int i = 0; i<32; i++){
            hashMapList.put(String.valueOf(ALFABE.charAt(i)),stringList.get(i));
        }
        return hashMapList;
    }
    public static HashMap<String,String> getStringHashpMapSolve(List<String> stringList){
        HashMap<String,String> hashMapList = new HashMap<String, String>();

        for (int i = 0; i<32; i++){
            hashMapList.put(stringList.get(i),String.valueOf(ALFABE.charAt(i)));
        }
        return hashMapList;
    }


















}
