package yerDegistimeKelime;



import java.util.Scanner;

public class App {
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.print("Kelime Gir :");
        String kelime = scanner.nextLine();
        System.out.println();
        System.out.print("Anahtar Kelime  Gir :");
        String anahtarKelime = scanner.nextLine();
        int uzunluk = anahtarKelime.length();

        şifrele(kelime,uzunluk);
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.print("Çözülmesi Gereken Kelimeyi  Gir :");
        Scanner scanner2 = new Scanner(System.in);
        String sifrelenmisMetin = scanner2.nextLine();
        şifreleCoz(sifrelenmisMetin,uzunluk);
        System.out.println();
        System.out.println();



    }
    public static void şifreleCoz(String kelime, int uzunluk){
        System.out.println();
        if (kelime.length() % uzunluk == 0){
            int situn = kelime.length() / uzunluk ;
            int satir = uzunluk;
            int sayac = 0 ;
            char[][] matris = new char[satir][situn];
            for (int i = 0 ; i < satir ; i++){
                for (int j = 0 ; j< situn ; j++){
                    matris[i][j] = kelime.charAt(sayac);
                    System.out.print(matris[i][j]);
                    sayac++;
                }
                System.out.println();
            }
            System.out.println();
            System.out.println();
            System.out.print("Çözülmüş Metin : ");
            for (int i = 0 ; i < situn ; i++){
                for (int j = 0; j< satir ; j++){
                    System.out.print(matris[j][i]);
                }
            }

        }
        else {
            System.out.println("Kelime uzunlugu ile uzunlugu bölüm tam olması gerekir. ");
            System.out.println("Kelime :" + kelime.length() + "  Uzunluk : " + uzunluk);
        }
    }
    public static void şifrele(String kelime, int uzunluk){
        if (kelime.length() % uzunluk == 0){
            int satir = kelime.length() / uzunluk ;
            int situn = uzunluk;
            int sayac = 0 ;
            char[][] matris = new char[satir][situn];
            for (int i = 0 ; i < satir ; i++){
                for (int j = 0 ; j< situn ; j++){
                    matris[i][j] = kelime.charAt(sayac);
                    System.out.print(matris[i][j]);
                    sayac++;
                }
                System.out.println();
            }
            System.out.println();
            System.out.println();
            System.out.print("Şifrelenmiş Metin : ");
            for (int i = 0 ; i < situn ; i++){
                for (int j = 0; j< satir ; j++){
                    System.out.print(matris[j][i]);
                }
            }

        }
        else {
            System.out.println("Kelime uzunlugu ile uzunlugu bölüm tam olması gerekir. ");
            System.out.println("Kelime :" + kelime.length() + "   Uzunluk : " + uzunluk);

        }
    }

}
