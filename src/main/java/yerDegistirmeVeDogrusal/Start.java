package yerDegistirmeVeDogrusal;


import yerDegistirmeVeDogrusal.service.MainService;
import yerDegistirmeVeDogrusal.service.MainServiceImpl;
import yerDegistirmeVeDogrusal.service.dogrusal.DogrusalService;
import yerDegistirmeVeDogrusal.service.dogrusal.DogrusalServiceImpl;
import yerDegistirmeVeDogrusal.service.yerDegistrime.YerDegistrimeService;
import yerDegistirmeVeDogrusal.service.yerDegistrime.YerDegistrimeServiceImpl;

import java.util.Scanner;

public class Start {
    MainService mainService = new MainServiceImpl();
    DogrusalService dogrusalService = new DogrusalServiceImpl();
    YerDegistrimeService yerDegistrimeService = new YerDegistrimeServiceImpl();

    public void start() {
        Scanner sc = new Scanner(System.in);

        while (true) {
            String word = mainService.getWord(sc);
            int chooseMethod = mainService.chooseMethod(sc);

            if (chooseMethod == 1) {

                switch (mainService.getSecenekSifrelemeIcin()) {
                    case 1:
                        dogrusalService.dogrusalSifrele(word);
                        break;
                    case 2:
                        yerDegistrimeService.yerineKoymaSifreleme(word);
                        break;
                }
            }
            if (chooseMethod == 2) {
                switch (mainService.getSecenekCozmekIcin()) {
                    case 1:
                        dogrusalService.dogrusalSifreCoz(word);
                        break;
                    case 2:
                        yerDegistrimeService.yerineKoymaSifreCoz(word);
                        break;
                }
            }
        }
    }
}
