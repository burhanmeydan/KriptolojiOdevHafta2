package yerDegistirmeVeDogrusal.service;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public interface MainService {
     String getWord(Scanner scanner);
     int chooseMethod(Scanner scanner);
     List<Character> addCharacterListByWord(String word);
     Map<String, Integer> getHashMapForString();
     Map<Integer, String > getHashMapForInteger();
     int getSecenekSifrelemeIcin();
     int getSecenekCozmekIcin();

}
