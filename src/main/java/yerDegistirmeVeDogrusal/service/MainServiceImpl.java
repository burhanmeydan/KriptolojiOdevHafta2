package yerDegistirmeVeDogrusal.service;

import java.util.*;

public class MainServiceImpl implements MainService {

    @Override
    public String getWord(Scanner scanner) {
        scanner  = new Scanner(System.in);
        System.out.println(" Metin Gir : ");
        String word = scanner.nextLine();
        return word;
    }

    @Override
    public int chooseMethod(Scanner scanner) {
        scanner  = new Scanner(System.in);
        System.out.println("Şifrelemek için -> 1");
        System.out.println("Şifre Çözmek için -> 2");
        int choose = scanner.nextInt();
        return choose;
    }

    @Override
    public List<Character> addCharacterListByWord(String word) {
        List<Character> characters = new ArrayList<Character>();
        for (int i = 0; i < word.length() ; i++){
            characters.add(word.charAt(i));
        }
        return characters;
    }

    @Override
    public Map<String, Integer> getHashMapForString() {
        Map<String, Integer> stringMap = new HashMap<String, Integer>();
        stringMap.put  ( "a",0 );
        stringMap.put  ( "b",1 );
        stringMap.put  ( "c",2 );
        stringMap.put  ( "ç",3 );
        stringMap.put  ( "d",4 );
        stringMap.put  ( "e",5 );
        stringMap.put  ( "f",6 );
        stringMap.put  ( "g",7 );
        stringMap.put  ( "ğ",8 );
        stringMap.put  ( "h",9);
        stringMap.put  ( "ı",10);
        stringMap.put  ( "i",11);
        stringMap.put  ( "j",12);
        stringMap.put  ( "k",13);
        stringMap.put  ( "l",14);
        stringMap.put  ( "m",15);
        stringMap.put  ( "n",16);
        stringMap.put  ( "o",17);
        stringMap.put  ( "ö",18);
        stringMap.put  ( "p",19);
        stringMap.put  ( "r",20);
        stringMap.put  ( "s",21);
        stringMap.put  ( "ş",22);
        stringMap.put  ( "t",23);
        stringMap.put  ( "u",24);
        stringMap.put  ( "ü",25);
        stringMap.put  ( "v",26);
        stringMap.put  ( "y",27);
        stringMap.put  ( "z",28);
        return  stringMap;
    }

    @Override
    public Map<Integer, String> getHashMapForInteger() {
        Map<Integer, String> stringMap = new HashMap<Integer, String>();
        stringMap.put  (0 ,"a" );
        stringMap.put  (1 ,"b" );
        stringMap.put  (2 ,"c" );
        stringMap.put  (3 ,"ç" );
        stringMap.put  (4 ,"d" );
        stringMap.put  (5 ,"e" );
        stringMap.put  (6 ,"f" );
        stringMap.put  (7 ,"g" );
        stringMap.put  (8 ,"ğ" );
        stringMap.put  (9 ,"h" );
        stringMap.put  (10,"ı" );
        stringMap.put  (11,"i" );
        stringMap.put  (12,"j" );
        stringMap.put  (13,"k" );
        stringMap.put  (14,"l" );
        stringMap.put  (15,"m" );
        stringMap.put  (16,"n" );
        stringMap.put  (17,"o" );
        stringMap.put  (18,"ö" );
        stringMap.put  (19,"p" );
        stringMap.put  (20,"r" );
        stringMap.put  (21,"s" );
        stringMap.put  (22,"ş" );
        stringMap.put  (23,"t" );
        stringMap.put  (24,"u" );
        stringMap.put  (25,"ü" );
        stringMap.put  (26,"v" );
        stringMap.put  (27,"y" );
        stringMap.put  (28,"z" );
        return  stringMap;
    }

    @Override
    public int getSecenekSifrelemeIcin() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Şifrelenme Çeşitleri : ");
        System.out.println("Doğrusal Şifreleme -> 1");
        System.out.println("Yerine Koyma Şifreleme -> 2");
        int secenek  = scanner.nextInt();
        return secenek;
    }

    @Override
    public int getSecenekCozmekIcin() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Şifre Çözme Çeşitleri : ");
        System.out.println("Doğrusal Şifreleme -> 1");
        System.out.println("Yerine Koyma Şifreleme -> 2");
        int secenek  = scanner.nextInt();
        return secenek;
    }
}
