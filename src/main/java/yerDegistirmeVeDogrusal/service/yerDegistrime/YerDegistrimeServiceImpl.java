package yerDegistirmeVeDogrusal.service.yerDegistrime;


import yerDegistirmeVeDogrusal.service.MainService;
import yerDegistirmeVeDogrusal.service.MainServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YerDegistrimeServiceImpl implements YerDegistrimeService {

    MainService mainService = new MainServiceImpl();
    @Override
    public List<String> yerineSifrelemeList(String word) {

        List<String> stringList = new ArrayList<String>();
        List<Character> characterList = mainService.addCharacterListByWord(word);
        Map<String, String> mapList = getMapSifreleForYerineKoyma();
        for (Character character : characterList){
            if (mapList.containsKey(character.toString())){
                stringList.add(mapList.get(character.toString()));
            }
        }
        return stringList;
    }

    @Override
    public void yerineKoymaSifreleme(String word) {
        List<String> stringList = yerineSifrelemeList(word);
        System.out.print("Yerine degistirme Şifrelenmiş Metin  : ");
        for (String string : stringList){
            System.out.print(string);
        }
        System.out.println();
        System.out.println("-------");
    }

    @Override
    public void yerineKoymaSifreCoz(String word) {
        List<String> stringList = yerineSifreCozmeList(word);
        System.out.print("Yerine degistirme Şifresi Çözülmüş Metin  : ");
        for (String string : stringList){
            System.out.print(string);
        }
        System.out.println();
        System.out.println("-------");
    }

    @Override
    public Map<String, String> getMapSifreleForYerineKoyma() {
        Map<String, String> stringMap = new HashMap<String, String>();
        stringMap.put  ( "a","z");
        stringMap.put  ( "b","y");
        stringMap.put  ( "c","v");
        stringMap.put  ( "ç","ü");
        stringMap.put  ( "d","u");
        stringMap.put  ( "e","t");
        stringMap.put  ( "f","ş");
        stringMap.put  ( "g","s");
        stringMap.put  ( "ğ","r");
        stringMap.put  ( "h","p");
        stringMap.put  ( "ı","ö");
        stringMap.put  ( "i","o");
        stringMap.put  ( "j","n");
        stringMap.put  ( "k","m");
        stringMap.put  ( "l","l");
        stringMap.put  ( "m","k");
        stringMap.put  ( "n","j");
        stringMap.put  ( "o","i");
        stringMap.put  ( "ö","ı");
        stringMap.put  ( "p","h");
        stringMap.put  ( "r","ğ");
        stringMap.put  ( "s","g");
        stringMap.put  ( "ş","f");
        stringMap.put  ( "t","e");
        stringMap.put  ( "u","d");
        stringMap.put  ( "ü","ç");
        stringMap.put  ( "v","c");
        stringMap.put  ( "y","b");
        stringMap.put  ( "z","a");
        stringMap.put  ("x","w");
        stringMap.put  ("w","q");
        stringMap.put  ("q","x");
        return stringMap;
    }

    @Override
    public Map<String, String> getMapSifreCozForYerineKoyma() {
        Map<String, String> stringMap = new HashMap<String, String>();
        stringMap.put  ("z", "a");
        stringMap.put  ("y", "b");
        stringMap.put  ("v", "c");
        stringMap.put  ("ü", "ç");
        stringMap.put  ("u", "d");
        stringMap.put  ("t", "e");
        stringMap.put  ("ş", "f");
        stringMap.put  ("s", "g");
        stringMap.put  ("r", "ğ");
        stringMap.put  ("p", "h");
        stringMap.put  ("ö", "ı");
        stringMap.put  ("o", "i");
        stringMap.put  ("n", "j");
        stringMap.put  ("m", "k");
        stringMap.put  ("l", "l");
        stringMap.put  ("k", "m");
        stringMap.put  ("j", "n");
        stringMap.put  ("i", "o");
        stringMap.put  ("ı", "ö");
        stringMap.put  ("h", "p");
        stringMap.put  ("ğ", "r");
        stringMap.put  ("g", "s");
        stringMap.put  ("f", "ş");
        stringMap.put  ("e", "t");
        stringMap.put  ("d", "u");
        stringMap.put  ("ç", "ü");
        stringMap.put  ("c", "v");
        stringMap.put  ("b", "y");
        stringMap.put  ("a", "z");
        stringMap.put  ("w","x");
        stringMap.put  ("q","w");
        stringMap.put  ("x","q");
        return stringMap;
    }

    @Override
    public List<String> yerineSifreCozmeList(String word) {
        List<String> stringList = new ArrayList<String>();
        List<Character> characterList = mainService.addCharacterListByWord(word);
        Map<String, String> mapList = getMapSifreCozForYerineKoyma();
        for (Character character : characterList){
            if (mapList.containsKey(character.toString())){
                stringList.add(mapList.get(character.toString()));
            }
        }
        return stringList;
    }
}
