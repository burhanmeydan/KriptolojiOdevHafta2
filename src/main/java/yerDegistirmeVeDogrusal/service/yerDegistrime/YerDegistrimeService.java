package yerDegistirmeVeDogrusal.service.yerDegistrime;

import java.util.List;
import java.util.Map;

public interface YerDegistrimeService {

    List<String> yerineSifrelemeList(String word);
    void yerineKoymaSifreleme(String word);
    void yerineKoymaSifreCoz(String word);
    Map<String, String> getMapSifreleForYerineKoyma();
    Map<String, String > getMapSifreCozForYerineKoyma();
    List<String> yerineSifreCozmeList(String word);

}
