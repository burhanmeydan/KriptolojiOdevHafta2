package yerDegistirmeVeDogrusal.service.dogrusal;

import java.util.List;

public interface DogrusalService {
    List<Integer> dogrusalSifrelemeList(String word, int a, int b);
    void dogrusalSifrele(String word);
    void dogrusalSifreCoz(String word);
    int sifreleDenklem(int a, int b, int harfRakam);
    List<Integer> getNumbers();
    List<Integer> dogrusalSifreCozList(String word, int sayi1, int sayi2);
    int sifreCozDenklem(int harfRakam, int a, int b, String word);
}
