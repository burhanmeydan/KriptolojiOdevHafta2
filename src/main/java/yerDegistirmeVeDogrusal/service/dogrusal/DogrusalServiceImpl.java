package yerDegistirmeVeDogrusal.service.dogrusal;


import yerDegistirmeVeDogrusal.service.MainService;
import yerDegistirmeVeDogrusal.service.MainServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class DogrusalServiceImpl implements  DogrusalService{
    MainService mainService = new MainServiceImpl();
    private static Scanner scanner = null;

    @Override
    public List<Integer> dogrusalSifrelemeList(String word ,int sayi1, int sayi2) {
        List<Integer> integerList = new ArrayList<Integer>();
        List<Character> characters = mainService.addCharacterListByWord(word);
        Map<String,Integer> integerMap = mainService.getHashMapForString();
        for (Character character : characters){
            if (integerMap.containsKey(character.toString())){
                int a =   integerMap.get(character.toString());
                integerList.add(sifreleDenklem(sayi1,sayi2,a));
            }
        }
        return integerList;
    }

    @Override
    public void dogrusalSifrele(String word) {
        List<Integer> list = getNumbers();
        int a = list.get(0);
        int b = list.get(1);
        String sifrelenmisMetin = "";
        List<Integer> integerList = dogrusalSifrelemeList(word,a,b);
        Map<Integer, String > stringMap = mainService.getHashMapForInteger();
        for (Integer integer : integerList){
            if (stringMap.containsKey(integer)){

                sifrelenmisMetin  = sifrelenmisMetin + stringMap.get(integer );
            }
        }
        System.out.println("Dogrusal' a göre şifrelenmiş metin : " + sifrelenmisMetin);
    }

    @Override
    public void dogrusalSifreCoz(String word) {
        List<Integer> list = getNumbers();
        int a = list.get(0);
        int b = list.get(1);
        String sifrelenmisMetin = "";
        List<Integer> integerList = dogrusalSifreCozList(word,a,b);
        Map<Integer, String > stringMap = mainService.getHashMapForInteger();
        for (Integer integer : integerList){
            if (stringMap.containsKey(integer)){
                sifrelenmisMetin  = sifrelenmisMetin + stringMap.get(integer );
            }
        }
        System.out.println("Dogrusal' a göre şifrelenmiş metin : " + sifrelenmisMetin);
    }

    @Override
    public int sifreleDenklem(int a, int b, int harfRakam) {

        return (a * harfRakam + b) % 29;
    }


    @Override
    public List<Integer> getNumbers() {
        scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<Integer>();
        System.out.println("a rakamı girin : ");
        scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        System.out.println("b rakamını girin :");
        int b = scanner.nextInt();
        list.add(a);
        list.add(b);
        return list;

    }

    @Override
    public int sifreCozDenklem( int harfRakam, int a, int b, String word) {
        boolean control = true;
        harfRakam = harfRakam - b ;
        if (harfRakam < 0 ){
            harfRakam = (harfRakam % 29) + 29 ;
        }
        do {
            if (harfRakam % a != 0){
                harfRakam  += 29;

            }
            else {
                control = false;
            }
        }
        while (control);
        harfRakam = harfRakam / a ;
        return harfRakam;

    }

    @Override
    public List<Integer> dogrusalSifreCozList(String word, int sayi1, int sayi2) {
        List<Integer> integerList = new ArrayList<Integer>();
        List<Character> characters = mainService.addCharacterListByWord(word);
        Map<String,Integer> integerMap = mainService.getHashMapForString();
        for (Character character : characters){
            if (integerMap.containsKey(character.toString())){
                int  a =   integerMap.get(character.toString());
                integerList.add(sifreCozDenklem( a, sayi1,sayi2,word));
            }
        }
        return integerList;
    }
}
